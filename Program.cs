using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main()
        {
            // HPの上限と薬草の所持数をしてする      
            int hp = 100;
            int mw = 5;
            Console.WriteLine($"現在のHPは{hp}です。");
            Console.WriteLine("現在のHPは" + hp + "です。");
            /* リテラルの中で文字列と変数を同居させるには、$から開始して｛｝で囲う
             * もしくは加算の演算子を利用する */

            nl();

            Console.WriteLine($"現在の薬草の所持数は{mw}です。");
            Console.WriteLine("現在の薬草の所持数は" + mw + "です。");

            nl();

            // HPを減らす。
            hp -= 50;
            Console.WriteLine("落石ィ！！！！");
            Console.WriteLine($"現在のHPは{hp}です。");

            nl();

            // 薬草を使用する。
            Console.WriteLine("薬草を使用しますか？");
            Console.WriteLine($"現在の薬草の所持数は{mw}です。");
            Console.WriteLine("yesかnoを入力");

            string checkmw = Console.ReadLine();

            if (checkmw == "yes")
            {
                mw--;
                hp += 50;
                Console.WriteLine($"現在のHPは{hp}です。");
                Console.WriteLine($"現在の薬草の所持数は{mw}です。");
            }   else if (checkmw == "no")
            {
                Console.WriteLine($"現在の薬草の所持数は{mw}です。");
            }   else
            {
                Console.WriteLine("yesかnoを入力");
            }

            if (input.getkeydown(keycode.leftarrow))
            {
                transform.translate(-3, 0, 0);
            }

        }

        //改行のための関数を別個に作成。
        static void nl()
        {
            Console.WriteLine();
        }