# C#を勉強するにあたって

## 参考サイト

[Visual Studio Code ではじめての C# アプリケーションを作る - Qiita][link1]  
[Windows GUIプログラミング入門 連載一覧 - Qiita][link2]

[link1]:https://qiita.com/nskydiving/items/4127c96e9588016566fe  
[link2]:https://qiita.com/Kosen-amai/items/f9e3df2aa80363f5af5b#windows-gui%E3%83%97%E3%83%AD%E3%82%B0%E3%83%A9%E3%83%9F%E3%83%B3%E3%82%B0%E5%85%A5%E9%96%80-%E9%80%A3%E8%BC%89%E4%B8%80%E8%A6%A7